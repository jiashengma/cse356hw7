const PORT = 8080;
const express = require('express');
const app = express();
const bodyParser = require('body-parser'); 
const mysql = require('mysql');
const Memcached = require('memcached');
const LIFETIME = 3600;

// const memc_locations = ['130.245.170.160:11211', '130.245.170.126:11211'];
// const memc_locations = '130.245.170.160:11211';
const memc_locations = 'localhost:11211';
const memc_options = {
    timeout:3000, // timeout after 3 sec
    maxExpiration: 3600, // cache expires after 1 hr
    
};
const memcached = new Memcached(memc_locations, memc_options);

// app.use(bodyParser.urlencoded({extended: true}));
// app.use(bodyParser.json());

const conn = mysql.createConnection({
    // connecting from 130.245.170.126
    host: "130.245.170.160",
    user: "ubuntu",
    password: "password4openstack",
    database: "hw7"
});

app.get('/hw7', (req, res)=>{

    let club = req.query.club;
    let pos = req.query.pos;
    
    // query memcached first 
    let memc_key = club+'_'+pos;
    console.log(`get: ${memc_key}`);
    memcached.get(memc_key, (err, data)=>{
        // if (err) {
        //     console.error(err);
        //     memcached.end();
        //     res.json({status:'error', error:err});
        // } else
        if (data) {
            console.log(`memcache hit: \nkey: ${memc_key}, \nvalue: `);
            // console.dir(data);
            res.json(data);
            memcached.end();
        } else {
            // query mysql and cache
            let sql = 'SELECT club, pos, A as max_assists, player, '
            +'(select cast(sum(A)/count(A) as decimal(3,2)) '
            +'FROM assists where club=? and pos=?) as avg_assists '
            + 'FROM assists where club=? and pos=? order by A desc, GS desc, player asc limit 1';
            let values = [club, pos, club, pos];
            conn.query(sql, values, (err, result) => {
                if (err) {
                    res.json({status:'error', error:err});
                } else {
                    if (result) {
                        // cache
                        memcached.set(memc_key, result[0], LIFETIME, (err) => {
                            memcached.end();
                        });
                        res.json(result[0]);
                    }
                    else {
                        res.json({status:'error', error:'empty'});
                    }
                }
            });
        }
    });

    
});

const server = app.listen(PORT, "0.0.0.0", () => {;
    console.log("hw7 listening at %s", PORT);
});
